#!/bin/bash

### unattended linux vm creation not supported

VM=$1

#Create a 10GB “dynamic” disk.
VBoxManage createhd --filename $VM.vdi --size 12500 --variant Fixed 

#create VM
VBoxManage createvm --name $VM --ostype "Linux_64" --register

#Add a SATA controller with the dynamic disk attached.

VBoxManage storagectl $VM --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach $VM --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VM.vdi

#Add an IDE controller with a DVD drive attached, and the install ISO inserted into the drive:
VBoxManage storagectl $VM --name "IDE Controller" --add ide
VBoxManage storageattach $VM --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium /Users/dmestry/Documents/Projects/7.iso

#Misc system settings.

VBoxManage modifyvm $VM --ioapic on
VBoxManage modifyvm $VM --boot1 dvd --boot2 disk --boot3 none --boot4 none
VBoxManage modifyvm $VM --memory 2048 --vram 128
VBoxManage modifyvm $VM --nic1 bridged --bridgeadapter1 en0

#check status 
if ( VBoxManage showvminfo $VM --machinereadable | grep 'VMState="running"' | wc -l > 0 ); then
	echo "$VM is created"
	echo "powering up $VM"
	VBoxManage startvm $VM 
fi




