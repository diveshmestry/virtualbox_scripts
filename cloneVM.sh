#!/bin/bash

VM=$1
cloneName=$2

#get list of VMs

vmlist=($(VBoxManage list vms))

# get length of an array
listLength=${#vmlist[@]}

#try to find VM
found=0
# use for loop read all nameservers
for (( i=0; i < ${listLength}; i++ ));do
	if [[ ${vmlist[i]} =~ $cloneName ]];then
		echo "vm already exists, choose another name"
		let found=1
		break
	else
		found=0
	fi	
done

#if VM is not found, create it
if [ $found -eq 0	];then
	echo "vm not found"
	echo "creating vm"
	
	VBoxManage clonevm $VM --name "$cloneName" --register

	VBoxManage startvm $cloneName

	#check status 
	if ( VBoxManage showvminfo $cloneName --machinereadable | grep 'VMState="running"' | wc -l > 0 ); then
		echo "$cloneName is created"
	fi
fi
