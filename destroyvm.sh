VMName=$1


vmlist=($(VBoxManage list vms))

# get length of an array
listLength=${#vmlist[@]}

found=0
# use for loop read all nameservers
for (( i=0; i < ${listLength}; i++ ));do
	if [[ ${vmlist[i]} =~ $VMName ]];then
		echo "vm exists"
		let found=1
		break
	else
		found=0
	fi	
done


if [ $found -eq 1	];then
	echo "vm found"
	echo "destroying vm"
	if ( VBoxManage showvminfo $VMName --machinereadable | grep 'VMState="running"' | wc -l > 0 ); then
		VBoxManage controlvm $VMName acpipowerbutton
		VBoxManage unregistervm $VMName --delete
	else
		VBoxManage unregistervm $VMName --delete
	fi
fi
